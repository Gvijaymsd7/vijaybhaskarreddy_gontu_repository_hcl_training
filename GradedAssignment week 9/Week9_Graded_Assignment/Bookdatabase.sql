create database week9;

use week9;

create table book(bookid int primary key,booktitle varchar(40), bookauthor varchar(50),bookimage varchar(10000),bookprice float);
insert into book values(1, "The little prince", "Clayton Chrisrensen","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/The-Little-Prince-book-cover.jpg?resize=768,1084&quality=82&strip=all",800);
insert into book values(2, "The Great Gatsby",  "Tracy Kidder","https://m.media-amazon.com/images/M/MV5BMTkxNTk1ODcxNl5BMl5BanBnXkFtZTcwMDI1OTMzOQ@@._V1_.jpg",1000);
insert into book values(3, "Harrey potter",  "Richard","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Don-Quixote-cover.jpg?resize=768,1177&quality=82&strip=all",500);
insert into book values(4, "The lord of the rings", "Thomas Friedman","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Lord-of-the-Rings-cover.jpg?resize=768,1155&quality=82&strip=all",700);
insert into book values(5, "Tale of Two cities", "Thomas Friedman","https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/A-Tale-of-Two-Cities-Cover.jpg?resize=768,1171&quality=82&strip=all",800);
insert into book values(6, "The Silent Patient", "TR Reid","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw8eeYcUICY3fyGrdpCI_AcCPNuVLrU9hgGA&usqp=CAU.jpg",1400);
insert into book values(7, "Don-Quixote",  "https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Don-Quixote-cover.jpg?resize=768,1177&quality=82&strip=all",5000);
insert into book values(8, "Telecosm", "George Gilder","https://venturebeat.com/wp-content/uploads/2012/04/telecosm.jpg?resize=200%2C271&strip=all",1500);
insert into book values(9, "The Wisdom of Crowds",  "James Surowiecki","https://venturebeat.com/wp-content/uploads/2012/04/the-wisdom-of-crowds.jpg?resize=200%2C277&strip=all",1800);

select * from book;


create table likedbook(bookid int primary key,booktitle varchar(40), bookauthor varchar(50),bookimage varchar(10000),bookprice float);
insert into book values(1, "The Silent Patient", "TR Reid","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRw8eeYcUICY3fyGrdpCI_AcCPNuVLrU9hgGA&usqp=CAU.jpg",1400);
insert into book values(2, "Don-Quixote",  "https://bestlifeonline.com/wp-content/uploads/sites/3/2020/10/Don-Quixote-cover.jpg?resize=768,1177&quality=82&strip=all",5000);

select * from likedbook;


create table user(userid int primary key,username varchar(20),email varchar(50),phonenumber long);

insert into user values(1,"vijay","vijay@gmail.com",9133750326);
insert into user values(2,"kumar","kumar@gmail.com",9848233797);

select * from user;

create table login(loginid int primary key,username varchar(30),password varchar(40));
insert into login values(1,"vijay","vijay@777");
insert into login values(2,"kumar","kumar@777");

select * from login;






