package com.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages="com.assignment")
@EntityScan(basePackages = "com.assignment.bean")				
@EnableJpaRepositories(basePackages = "com.assignment.dao")			

public class Week9GradedAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week9GradedAssignmentApplication.class, args);
		System.err.println("Server running on port 8081");
		
	}

}
