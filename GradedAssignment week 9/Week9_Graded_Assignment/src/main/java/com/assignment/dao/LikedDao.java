package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.bean.LikedBook;

@Repository
public interface LikedDao extends JpaRepository<LikedBook, Integer>{

}
