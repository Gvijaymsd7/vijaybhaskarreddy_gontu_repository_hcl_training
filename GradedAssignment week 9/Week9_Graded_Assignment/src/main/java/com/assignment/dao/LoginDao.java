package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.bean.Login;

@Repository
public interface LoginDao extends JpaRepository<Login,Integer>{

}
