package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.bean.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer>{

}
