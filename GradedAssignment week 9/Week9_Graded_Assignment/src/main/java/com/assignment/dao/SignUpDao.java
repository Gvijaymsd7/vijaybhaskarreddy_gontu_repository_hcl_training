package com.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment.bean.SignUp;

@Repository
public interface SignUpDao extends JpaRepository<SignUp, Integer>{

}
