package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.bean.Book;
import com.assignment.dao.BookDao;

@Service
public class BookService {
	
	@Autowired
	BookDao bookDao;
	
	public String storeBookInfo(Book book)
	{	
		if(bookDao.existsById(book.getBookId())) 
		{
		  return "BookId must be unique";
		}else {
		  bookDao.save(book);
		  return "Book stored successfully";
		}
    }
	
	public List<Book> getAllBooksInfo(){
		return bookDao.findAll();
	}
	
	
	public String updateBook(Book book) {
		if(!bookDao.existsById(book.getBookId())) 
		{
		    return "Book is not present with the given id";
		}
		else 
		{
			Book b	= bookDao.getById(book.getBookId()); 
			b.setBookPrice(book.getBookPrice());					
			bookDao.saveAndFlush(b);				
			return "Book details updated successfully";
		}	
	}
	
	public String deleteBook(int BookId)
	{
		if(!bookDao.existsById(BookId)) {
			return "Book is not present with given id";
			}else {
			bookDao.deleteById(BookId);
			return "Book deleted successfully";
			}	
	}

}
