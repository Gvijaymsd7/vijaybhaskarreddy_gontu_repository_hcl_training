package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.bean.Login;
import com.assignment.dao.LoginDao;

@Service
public class LoginService {
	
	@Autowired
	LoginDao loginDao;
	
	public String storeLoginDetails(Login login)
	{	
		if(loginDao.existsById(login.getLoginId())) 	
		{
		  return "LoginId is Unique for everyone";
		}else {
		  loginDao.save(login);
		  return "login successfull";
		}
    }
	public List<Login> getAllLoginDetails(){
		return loginDao.findAll();
	}

}
