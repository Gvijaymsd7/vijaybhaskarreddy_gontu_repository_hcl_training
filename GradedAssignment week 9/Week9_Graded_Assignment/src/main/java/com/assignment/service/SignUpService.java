package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.bean.SignUp;
import com.assignment.dao.SignUpDao;

@Service
public class SignUpService {
	
	@Autowired
	SignUpDao signUpDao;
	

	public String storeSignUpDetails(SignUp signup)
	{	
		if(signUpDao.existsById(signup.getSignUpId())) 
		{
		  return "SignUp Id is Unique";
		}else {
		  signUpDao.save(signup);
		  return "signup successfull";
		} 
    }
	public List<SignUp> getAllSignUpDetails(){
		return signUpDao.findAll();
	}

}
