package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.bean.LikedBook;
import com.assignment.dao.LikedDao;

@Service
public class LikedService {
	@Autowired
	LikedDao likedDao;
	
	public String storeLikedBookDetails(LikedBook likedBook)
	{	
		if(likedDao.existsById(likedBook.getBookId())) 
		{
		  return "BookId must be unique";
		}else {
		  likedDao.save(likedBook);
		  return "Books stored successfully";
		}
    }
	public List<LikedBook> getAllLikedBooks(){
		return likedDao.findAll();
	}
	
	public String deleteLikedBook(int BookId)
	{
		if(!likedDao.existsById(BookId)) {
			return "Book is not present with given id";
			}else {
			likedDao.deleteById(BookId);
			return " Liked Book deleted successfully";
			}	
	}

	
}
