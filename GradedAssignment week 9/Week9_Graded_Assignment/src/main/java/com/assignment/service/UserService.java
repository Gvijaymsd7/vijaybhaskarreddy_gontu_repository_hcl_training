package com.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment.bean.User;
import com.assignment.dao.UserDao;

@Service
public class UserService {
	
	@Autowired
	UserDao userDao;
	
	public String storeUserDetails(User user)
	{	
		if(userDao.existsById(user.getUserId())) 
		{
		  return "UserId must be unique";
		}else {
		  userDao.save(user);
		  return " User data stored successfully";
		}
    }
	
	public String deleteUserDetails(int userId)
	{
		if(!userDao.existsById(userId)) {
			return "UserId is not present";
			}else {
			userDao.deleteById(userId);
			return "UserId deleted successfully";
			}	
	}
	
	public String updateUserDetails(User user) {
		if(!userDao.existsById(user.getUserId())) 
		{
		    return "userId is not present with the given data";
		}
		else 
		{
			User us	= userDao.getById(user.getUserId()); 
			us.setPhoneNumber(user.getPhoneNumber());					
			userDao.saveAndFlush(us);				
			return " User details updated successfully";
		}	
	}
	
	public List<User> getAllUsers(){
		return userDao.findAll();
	}

}
