package com.assignment.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "signup")
public class SignUp {
	
	@Id
	@Column(name = "signupid")
	private int signUpId;
	@Column(name = "username")
	private String userName;
	@Column(name = "emailid")
	private String emailId;
	@Column(name = "password")
	private String password;
	public int getSignUpId() {
		return signUpId;
	}
	public void setSignUpId(int signUpId) {
		this.signUpId = signUpId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return "SignUp [signUpId=" + signUpId + ", userName=" + userName + ", emailId=" + emailId + ", password="
				+ password + "]";
	}

	
	
}
