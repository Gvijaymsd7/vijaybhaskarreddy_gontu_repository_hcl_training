package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bean.Book;
import com.assignment.service.BookService;


@RestController
@RequestMapping(value = "Book")
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@GetMapping(value = "getAllBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Book> getAllBookDetails()
	{
	return bookService.getAllBooksInfo();
	}
	@PostMapping(value = "storeBookInfo",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeBookDetails(@RequestBody Book book) 
	{	
	return bookService.storeBookInfo(book);
	}
	
	@DeleteMapping(value = "deleteBook/{bookid}")
	public String deleteBookDetails(@PathVariable("bookid") int BookId)
	{
	return bookService.deleteBook(BookId);
	}
	
	@PatchMapping(value = "updateBook")
	public String updateBookDetails(@RequestBody Book book) 
	{
	return bookService.updateBook(book);
	}

}
