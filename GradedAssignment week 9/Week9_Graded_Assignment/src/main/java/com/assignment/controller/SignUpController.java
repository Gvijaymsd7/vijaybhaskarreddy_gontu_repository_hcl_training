package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bean.SignUp;
import com.assignment.service.SignUpService;

@RestController
@RequestMapping(value = "signup")
public class SignUpController {
	
	@Autowired
	SignUpService signService;
	
	@PostMapping(value = "storeSignUpDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeSignupInfo(@RequestBody SignUp signup) 
	{	
	return signService.storeSignUpDetails(signup);
	}
	@GetMapping(value = "getAllSignUpDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SignUp> getAllSignUpDetails()
	{
	return signService.getAllSignUpDetails();
	}

}
