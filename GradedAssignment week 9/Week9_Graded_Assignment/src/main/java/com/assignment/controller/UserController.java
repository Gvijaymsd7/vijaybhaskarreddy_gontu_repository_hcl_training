package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bean.User;
import com.assignment.service.UserService;

@RestController
@RequestMapping(value = "user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@PostMapping(value = "storeUserDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeUserInfo(@RequestBody User user) 
	{	
	return userService.storeUserDetails(user);
	}
	
	@DeleteMapping(value = "deleteUserDetails/{userId}")
	public String deleteUserInfo(@PathVariable("userId") int userId)
	{
	return userService.deleteUserDetails(userId);
	}
	
	@PatchMapping(value = "updateUserDetails")
	public String updateUserInfo(@RequestBody User user) 
	{
	return userService.updateUserDetails(user);
	}
	@GetMapping(value = "getAllUserDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<User> getAllUserDetails()
	{
	return userService.getAllUsers();
	}

	
	

}
