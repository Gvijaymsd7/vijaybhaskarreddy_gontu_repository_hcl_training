package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bean.Login;
import com.assignment.service.LoginService;

@RestController
@RequestMapping(value = "login")
public class LoginController {
	
	@Autowired
	LoginService loginService;
	
	@PostMapping(value = "storeLoginDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeLoginInfo(@RequestBody Login login) 
	{	
	return loginService.storeLoginDetails(login);
	}
	@GetMapping(value = "getAllLoginDetails",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Login> getAllLoginDetails()
	{
	return loginService.getAllLoginDetails();
	}
	

}
