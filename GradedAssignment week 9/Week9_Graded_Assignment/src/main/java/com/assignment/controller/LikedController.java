package com.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bean.LikedBook;
import com.assignment.service.LikedService;

@RestController
@RequestMapping(value = "LikedBook")
public class LikedController {
	
	@Autowired
	LikedService likeService;
	@PostMapping(value = "storeLikedBookDetails",consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeProductInfo(@RequestBody LikedBook likedBook) 
	{	
	return likeService.storeLikedBookDetails(likedBook);
	}
	@GetMapping(value = "getAllLikedBooks",produces = MediaType.APPLICATION_JSON_VALUE)
	public List<LikedBook> getAllBookDetails()
	{
	return likeService.getAllLikedBooks();
	}
	
	@DeleteMapping(value = "deleteLikedBook/{bookid}")
	public String deleteBookDetails(@PathVariable("bookid") int BookId)
	{
	return likeService.deleteLikedBook(BookId);
	}
}
