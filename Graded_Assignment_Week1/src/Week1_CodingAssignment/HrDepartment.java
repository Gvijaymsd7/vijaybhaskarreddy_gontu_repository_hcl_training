package Week1_CodingAssignment;

public class HrDepartment extends SuperDepartment
{
	//declare method departmentName of return type string
	public String hrDepartment()
	{
		return "HR Department";
	}
	//declare method getTodaysWork of return type string
	public String getTodaysWork()
	{
		return "Fill todays worksheet and mark your attendance";
	}
	//declare method getWorkDeadline of return type string
	public String getWorkDeadline()
	{
		return "Complete by the End Of The Day";
	}
	
	//declare method doActivity of return type string
	public String doActivity()
	{
		return "Team Lunch";
	}
}
