package Week1_CodingAssignment;
import java.util.Scanner;

public class Main {

	//

	public static void main(String[] args) 
	 {
		Scanner sc=new Scanner(System.in);
		System.out.println("choose the department:");
		
		System.out.println("1.enter 'super' for Super department\n"+
		"2.enter 'Admin' for Admin department\n"+
				"3.enter 'Hr' for Hr department\n"+
		"4.enter 'Tech' for Tech department\n"); 
		
		String x=sc.next();
		switch(x)
		{
		
		case "super":
			         SuperDepartment sd=new SuperDepartment();
		             System.out.println("department name is :  "+sd.deparmentName());
		             System.out.println("today work is :       "+sd.getTodaysWork());
		             System.out.println( "work deadline is :   "+sd.getWorkDeadline());
		             System.out.println("update about holiday :"+sd.isTodayAHoliday());
		             break;
		
		case "Admin":
			         AdminDepartment ad = new AdminDepartment();
		             System.out.println("department name is :  "+ad.adminDepartment());
		             System.out.println("today work is :       "+ad.getTodaysWork());
		             System.out.println("work deadline is :    "+ad.getWorkDeadline());
		             System.out.println("update about holiday :"+ad.isTodayAHoliday());
		             break;
		case "Hr":
			      HrDepartment hd = new HrDepartment();
		          System.out.println("department name is :      "+hd.hrDepartment());
		          System.out.println("today work is :           "+hd.getTodaysWork());
		          System.out.println("work deadline is :        "+hd.getWorkDeadline());
		          System.out.println("update about holiday :    "+hd.isTodayAHoliday());
		          System.out.println("today activity is :       "+hd.doActivity());
		          break;
		case "Tech":
			        TechDepartment td = new TechDepartment();
			        System.out.println("department name is :    "+td.techDepartment());
			        System.out.println("today work is :         "+td.getTodaysWork());
			        System.out.println("work deadline is :      "+td.getWorkDeadline());
			        System.out.println("update about holiday :  "+td.isTodayAHoliday());
			        System.out.println("Tech Stack Information is :"+td.getTechStackInformation());
		            break;
		default   : 
					System.out.println("incorrect department name");

	}
	}
}
