package Week1_CodingAssignment;

public class AdminDepartment extends SuperDepartment
{
	//declare method departmentName of return type string
	public String adminDepartment()
	{
		return "Admin Department";
	}
	
	//declare method getTodaysWork of return type string
	public String getTodaysWork()
	{
		
		return "Complete your documents";
	}
	//declare method getWorkDeadline of return type string
	public String getWorkDeadline()
	{
		return "Complete by the End Of The Day";
	}
}
