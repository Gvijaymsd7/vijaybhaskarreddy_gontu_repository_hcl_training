package Week1_CodingAssignment;

public class TechDepartment extends SuperDepartment
{
	//declare method departmentName of return type string
		public String techDepartment()
		{
			return "Tech Department";
		}
		//declare method getTodaysWork of return type string
		public String getTodaysWork()
		{
			return "Complete coding of module 1";
		}
		//declare method getWorkDeadline of return type string
		public String getWorkDeadline()
		{
			return "Complete by the End Of The Day";
		}
	    //declare method getTechStackInformation of return type string
		public String getTechStackInformation()
		{
			return "core java";
		}
  
}
