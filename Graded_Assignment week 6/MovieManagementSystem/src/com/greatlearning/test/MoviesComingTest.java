package com.greatlearning.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.Movies;
import com.greatlearning.moviescomingdao.MoviesComing;

public class MoviesComingTest {

	@Test
	public void testGetMovies() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		List<Movies> lo= mc.getMovies();
		
		assertEquals(10, lo.size());
	}

	@Test
	public void testInsert() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movies m=new Movies();
		m.setId(1);
		m.setTitle("narniya");
		int success=mc.insert(m);
		assertEquals(1,success);
	}

	@Test
	public void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movies m=new Movies();
		m.setId(1);
		m.setTitle("bhahubali");
		mc.update(m);
		assertTrue(m.getTitle()=="bhahubali");
		
		
	}

	@Test
	public void testDelete() throws SQLException {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		Movies m=new Movies();
		m.setTitle("dangal");
		mc.delete(m);
		assertTrue(m.getTitle()=="dangal");
	}

}
