package com.greatlearning.moviescomingdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.GenresAndRating;
import com.greatlearning.interfaces.AllGenresAndRating;
//import com.greatlearning.interfaces.AllMovies;
//import com.greatlearning.bean.Movies;
import com.greatlearning.resource.DbResource;

public class GenresRatingMC implements AllGenresAndRating{
	public List<GenresAndRating> getMovies() throws SQLException 
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("select *from moviescoming_genres_rating");
		ResultSet rs = ps.executeQuery();
		List<GenresAndRating> ls = new ArrayList<>();

		while (rs.next()) {
			
			GenresAndRating emp = new GenresAndRating();
			emp.setId(rs.getInt("id"));
			emp.setGenres(rs.getString("genres"));
			emp.setRating(rs.getInt("rating"));
			emp.setGid(rs.getInt("gid"));
			ls.add(emp);
		}
		return ls;
	}catch(Exception e) {
		System.out.println(e);
		return null;
	}
	}

	public int insert(GenresAndRating movie) throws SQLException
	{
		try {
			
			Connection con = DbResource.getDbConnection();
		
		PreparedStatement ps = con.prepareStatement("insert into moviescoming_genres_rating values(?,?,?,?,?,?,?,?,?)");

		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getGenres());
		ps.setInt(3, movie.getRating());
		ps.setInt(4, movie.getGid());
		
		return ps.executeUpdate();
		}catch(Exception e){
			System.out.println(e);
		
		}
		return 0;
	}
	 
	public int update(GenresAndRating movie) throws SQLException
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("update moviescoming_genres_rating set id=? where name=?");
		
		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getGenres());
		return ps.executeUpdate();
		}catch(Exception e)
		{
		System.out.println(e);
			return 0;	
		}
		
	}
	
	public int delete(GenresAndRating movie) throws SQLException
	
	{
		try {
			
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("delete from moviescoming_genres_rating where name=?");
		ps.setString(1, movie.getGenres());
		return ps.executeUpdate();
		}catch (Exception e){
			System.out.println(e);
			return 0;
		}
		
		
	
		}
}
