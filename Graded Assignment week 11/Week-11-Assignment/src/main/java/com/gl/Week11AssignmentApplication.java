package com.gl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Week11AssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Week11AssignmentApplication.class, args);
	}

}
