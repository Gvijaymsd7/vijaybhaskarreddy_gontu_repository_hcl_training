package com.MiniProject.Resturant.menu;

	public class ResturantMenu {
		private int ItemID;
		private String ItemName;
		private int ItemQuantity;
		private double ItemPrice;
		
		
		public ResturantMenu() {
			super();
			// TODO Auto-generated constructor stub
		}

		public ResturantMenu(int ItemID, String ItemName, int ItemQuantity, double ItemPrice) throws IllegalArgumentException
		{
			super();
			
			if(ItemID<0)
			{
				throw new IllegalArgumentException("exception occured, ID cannot less than zero");
			}
			this.ItemID = ItemID;
			
			if(ItemName==null) {
				throw new IllegalArgumentException("exception occured, name cannot be null");
			}
			this.ItemName = ItemName;
			
			if(ItemQuantity<0)
			{
				throw new IllegalArgumentException("exception occured, quantity cannot less than zero");
			}
			this.ItemQuantity = ItemQuantity;
			
			if(ItemPrice<0)
			{
				throw new IllegalArgumentException("exception occured, price cannot less than zero");
			}
			this.ItemPrice = ItemPrice;
		}

		public int getItemID() {
			return ItemID;
		}

		public void setItemID(int itemID) {
			this.ItemID = itemID;
		}

		public String getItemName() {
			return ItemName;
		}

		public void setItemName(String itemName) {
			this.ItemName = itemName;
		}

		public int getItemQuantity() {
			return ItemQuantity;
		}

		public void setItemQuantity(int itemQuantity) {
			this.ItemQuantity = itemQuantity;
		}

		public double getItemPrice() {
			return ItemPrice;
		}

		public void setItemPrice(double itemPrice) {
			this.ItemPrice = itemPrice;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return  ( ItemID +"\t\t"+ ItemName + "\t\t" + ItemQuantity + "\t\t" + ItemPrice);
		}

//		public static Iterable<Bill> stream() {
//			// TODO Auto-generated method stub
//			return null;
	//}
		
		

}
