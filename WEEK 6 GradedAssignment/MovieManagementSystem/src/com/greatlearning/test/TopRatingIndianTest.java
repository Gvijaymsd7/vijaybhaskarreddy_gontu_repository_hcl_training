package com.greatlearning.test;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.greatlearning.bean.Movies;
//import com.greatlearning.moviesintheatersdao.MoviesInTheater;
import com.greatlearning.topratingindiandao.TopRatingIndian;

public class TopRatingIndianTest {

	@Test
	public void testGetMovies() throws SQLException {
		//fail("Not yet implemented");
		TopRatingIndian mt=new TopRatingIndian();
		List<Movies> lo= mt.getMovies();
		
		assertEquals(50, lo.size());
	}

	@Test
	public void testInsert() throws SQLException {
		//fail("Not yet implemented");
		TopRatingIndian mt=new TopRatingIndian();
		Movies m=new Movies();
		m.setId(1);
		m.setTitle("narniya");
		int success=mt.insert(m);
		assertEquals(1,success);
	}

	@Test
	public void testUpdate() throws SQLException {
		//fail("Not yet implemented");
		TopRatingIndian mc=new TopRatingIndian();
		Movies m=new Movies();
		m.setId(1);
		m.setTitle("bhahubali");
		mc.update(m);
		assertTrue(m.getTitle()=="bhahubali");
		
		
	}

	@Test
	public void testDelete() throws SQLException {
		//fail("Not yet implemented");
		TopRatingIndian mc=new TopRatingIndian();
		Movies m=new Movies();
		m.setTitle("dangal");
		mc.delete(m);
		assertTrue(m.getTitle()=="dangal");
	}

	}

