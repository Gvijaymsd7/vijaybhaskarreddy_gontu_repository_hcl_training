package com.greatlearning.bean;
import org.springframework.stereotype.Component;

@Component
public class Book {

	private int bookId;
	private String name;
	private String authorName;
	private String type;
	private int pageCount;
	private String poster;

	public Book() {
	}
	
	public Book(int bookId, String name, String authorName ,String type, int pagecount, String poster) {
		super();
		this.bookId = bookId;
		this.name = name;
		this.authorName = authorName;
		this.type = type;
		this.pageCount = pagecount;
		this.poster = poster;
	}

	public int getBookId() {
		return bookId;
	}

	public void setBookId(int bookId) {
		this.bookId = bookId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor_name() {
		return authorName;
	}

	public void setAuthor_name(String author_name) {
		this.authorName = authorName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pagecount) {
		this.pageCount = pagecount;
	}
	
	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", name=" + name + ", authorName=" + authorName +", type=" + type + ", pagecount=" + pageCount 
				+ " poster: " +poster+"]";
	}

	public void setAuthorName(String string) {
		// TODO Auto-generated method stub
		
	}
	
	
	
}