package com.greatlearning.factory;

import com.greatlearning.interfaces.AllMovies;
import com.greatlearning.moviescomingdao.MoviesComing;
import com.greatlearning.moviesintheatersdao.MoviesInTheater;
import com.greatlearning.topratingindiandao.TopRatingIndian;
import com.greatlearning.topratingmoviesdao.TopRatingMovies;

public class AllMoviesFactory {
	public static AllMovies createAllMovies(String channel) {
		if (channel == null || channel.isEmpty())
			return null;
		if ("moviescoming".equals(channel)) {
			return new MoviesComing();
		} else if ("moviesinTheater".equals(channel)) {
			return new MoviesInTheater();
		} else if ("TopRatedIndia".equals(channel)) {
			return new TopRatingIndian();
		} else if ("topRatedMovies".equals(channel)) {
			return new TopRatingMovies();
		}

		return null;

	}
}
