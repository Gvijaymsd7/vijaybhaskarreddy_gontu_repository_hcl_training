package com.MiniProject.Resturant.Main;
    import com.MiniProject.Resturant.menu.*;
    import com.MiniProject.Resturant.Bills.*;
	import java.time.LocalDate;
	import java.time.ZonedDateTime;
	import java.time.format.DateTimeFormatter;
	import java.util.*;

public class Main 
{
		@SuppressWarnings("deprecation")
	  public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
		 Date time = new Date();
		 List<CustumersBills> bills = new ArrayList<CustumersBills>();
		 boolean finalOrder;
		 List<ResturantMenu> items = new ArrayList<ResturantMenu>();
		 
		 ResturantMenu i1 = new ResturantMenu(1, "Rajma Chawal", 2, 150.0);
		 ResturantMenu i2 = new ResturantMenu(2, "     Momos  ", 2, 190.0);
		 ResturantMenu i3 = new ResturantMenu(3, "Red Thai Curry", 2, 180.0);
		 ResturantMenu i4 = new ResturantMenu(4, "   Chaap     ", 2, 190.0);
		 ResturantMenu i5 = new ResturantMenu(5, "Chilli Potato", 1, 250.0);
		 
		 items.add(i1);
		 items.add(i2);
		 items.add(i3);
		 items.add(i4);
		 items.add(i5);
		 
		 System.out.println("Welcome to Surabi Restaurants \n++++++++++++++++"
		 		+ "++++++++++++++++++++++++++++++++++++++++++++++");
		 while(true) {
			 System.out.println("Please Enter Your Credentials");
			 
			 System.out.println("Email = ");
			 String email = sc.next();
			 
			 System.out.println("Password = ");
			 String password = sc.next();
			 
			 String name = Character.toUpperCase(password.charAt(0)) + password.substring(1);
		
			 System.out.println("Please Enter A if you are Admin and U if you are User, to logout");
			 String aorU = sc.next();
			 
			 CustumersBills bill = new CustumersBills();
			 List<ResturantMenu> selectedMenu = new ArrayList<ResturantMenu>();
			 
			 double totalCost = 0;
			 
			 Date date = new Date();
			 
			 ZonedDateTime time1 = ZonedDateTime.now();
			 DateTimeFormatter f = DateTimeFormatter.ofPattern("E MMM dd HH : mm:ss zzz yyyy");
			 String currentTime = time1.format(f);
			 
			 if(aorU.equals("U") || aorU.equals("U")) {
				 System.out.println("Welcome MR." + name);
				 //Object selectedItems;
				do {
					  System.out.println("Today's Menu :-");
					  items.stream().forEach(i -> System.out.println(i));
					  System.out.println("Enter the Menu Item code");
					  int code = sc.nextInt();
					  
					  if(code ==1) {
						  selectedMenu.add(i1);
						  totalCost += i1.getItemPrice();
					  }
					  else if (code ==2) {
						  selectedMenu.add(i2);
						  totalCost += i2.getItemPrice(); 
					  } else if (code == 3) {
						  selectedMenu.add(i3);
						  totalCost += i3.getItemPrice();
					  }else if (code == 4 ) {
						  selectedMenu.add(i4);
						  totalCost += i4.getItemPrice();
					  }else {
						  selectedMenu.add(i5);
						  totalCost += i5.getItemPrice();
					  }
					  
					  System.out.println("Press 0 to show bill\n Press 1 to order more");
					  
					  int opt = sc.nextInt();
						if (opt == 0)
							finalOrder = false;
						else
							finalOrder = true;

					} while (finalOrder);
					

					System.out.println("Thanks Mr " + name + " we hope you feel good about Surabi's food items ");
					System.out.println(".....Items you have Selected.....");
					System.out.println("***************************************************************************");
					System.out.println("itemId"+"\t\t"+"itemName"+"\t\t"+"itemQuantity"+"\t\t"+"itemPrice");
					System.out.println("----------------------------------------------------------------------------");
					selectedMenu.stream().forEach(e -> System.out.println(e));
					System.out.println("Your Total bill  is :" + totalCost);

				
					bill.setName(name);
					bill.setCost(totalCost);
					bill.setMenu(selectedMenu);
					bill.setTime(date);
					bills.add(bill);

				} else if (aorU.equals("A") || aorU.equals("a")) {
					System.out.println("Welcome Admin");
					System.out.println(
							"Press 1 to see all the bills for today\nPress 2 to see all the bills for this month\nPress 3 to see all the bills");
					int option = sc.nextInt();
					switch (option) {
					case 1:
						if ( !bills.isEmpty()) {
							for (CustumersBills b : bills) {
								if(b.getTime().getDate()==time.getDate()) {
								//if(b.getTime().getDate()==time.getDate()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills today.!");
						break;

					case 2:
						if (!bills.isEmpty()) {
							for (CustumersBills b : bills) {
								if (b.getTime().getMonth()==time.getMonth()) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
							}
						} else
							System.out.println("No Bills for this month.!");
						break;

					case 3:
						if (!bills.isEmpty()) {
							for (CustumersBills b : bills) {
								System.out.println("\nUsername :- " + b.getName());
								System.out.println("Items :- " + b.getMenu());
								System.out.println("Total :- " + b.getCost());
								System.out.println("Date " + b.getTime() + "\n");
							}
						} else
							System.out.println("No Bills.!");

						break;

					default:
						System.out.println("Invalid Option");
						System.exit(1);
					}
				} else if (aorU.equals("L") || aorU.equals("l")) {
					System.exit(1);
					
				}else  {
					System.out.println("Invalid Entry");
				}

		}

	}
}
