package com.hcl.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.project.dao.UserRepo;
import com.hcl.project.entities.User;



@Service
public class UserService {

	@Autowired
	UserRepo userRepo;

	

	public void save(User user) {
		userRepo.save(user);
	}

	public User login(String name, String password) {
		User user = userRepo.findByNameAndPassword(name, password);
		return user;
	}

	public List<User> showUser() {
		List<User> user = userRepo.findAll();
		return user;
	}
}
