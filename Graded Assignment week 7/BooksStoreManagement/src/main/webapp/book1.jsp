<%@page import="com.bean.Book"%>
<%@page import="java.util.List"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

</head>
<body>
<h2>G vijay bhaskar reddy</h2>
	<h3>
		<a href='login'>Login</a>
	</h3>
	<h3>
		<a href='register'>Register</a>
	</h3>
	<h1>Available Books</h1>
	<table border="1">
		<tr>
			<th>id</th>
			<th>title</th>
			<th>author</th>
			<th>year</th>
		</tr>
		<%
		List<Book> books = (List<Book>) request.getAttribute("list");
		for (Book book : books) {
		%>
		<tr>
			<td><p>
					id:<%=book.getId()%></p></td>
			<td><p>
					title:<%=book.getTitle()%></p></td>
			<td><p>
					author:<%=book.getAuthor()%></p></td>
			<td><p>
					year:<%=book.getYear()%></p></td>
		</tr>
		<%
		}
		%>
	</table>
</body>
</html>