package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bean.LoginUser;
import com.resource.DBConnection;

public class LoginUserDatabase {
	public boolean validate(LoginUser user)
	{
		String sql = "select email, password from bookscustomer where email = ?";
		// establishing connection
		Connection con = DBConnection.getConnection();
		PreparedStatement statement = null;
		try {
			statement = con.prepareStatement(sql);
			statement.setString(1, user.getEmail());
			ResultSet rs = statement.executeQuery();
			String useremail = null,userpassword = null;
			if(rs.next()) {
				useremail = rs.getString(1);
				userpassword = rs.getString(2);
				if(user.getEmail().equals(useremail) && user.getPassword().equals(userpassword)) {
					return true;
				} 
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return false;
	}
}
