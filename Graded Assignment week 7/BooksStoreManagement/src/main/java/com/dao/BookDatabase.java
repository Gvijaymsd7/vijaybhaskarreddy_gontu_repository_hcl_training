package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.bean.Book;
import com.resource.DBConnection;

public class BookDatabase {
	private static Connection con = DBConnection.getConnection();

	public List<Book> getAllItems() throws SQLException
	{
		List<Book> books = new ArrayList<>();
		String sql = "select  * from book";

		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));
			books.add(book);
		}
		return books;
	}
	public List<Book> getAllFav() throws SQLException
	{
		List<Book> books = new ArrayList<>();
		String sql = "select  * from favtable";

		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{
			Book book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));
			books.add(book);
		}
		return books;
	}
	public Book getBookById(int id) throws SQLException
	{
		Book book = null;
		String sql = "select  * from Book where id='"+id+"'";
		//  statement
		Statement statement = con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		if(rs.next())
		{
			book = new Book();
			book.setId(rs.getInt(1));
			book.setTitle(rs.getString(2));
			book.setAuthor(rs.getString(3));
			book.setYear(rs.getString(4));

		}
		System.out.println(book);
		return book;
	}
	public boolean favTable(String id) throws SQLException {

		String sql = "insert into favtable select * from book where id='"+id+"'";
		PreparedStatement statement = con.prepareStatement(sql);
		statement.executeUpdate();
		return true;

	}

}
