package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bean.Book;
import com.dao.BookDatabase;

public class Books {
	/**
	 * Servlet implementation class BookServlet
	 */
	@WebServlet("/books")
	public class BookServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		/**
		 * @see HttpServlet#HttpServlet()
		 */
		public BookServlet() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			HttpSession session = request.getSession();
			String email = (String) session.getAttribute("email");
			if(email != null) {
				BookDatabase database = new BookDatabase();
				try {
					List<Book> books = database.getAllItems();
					RequestDispatcher rd = request.getRequestDispatcher("book.jsp");
					request.setAttribute("list", books);
					rd.forward(request, response);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				response.sendRedirect("login.jsp"); 
			}
			String id = request.getParameter("id");
			BookDatabase bd = new BookDatabase();
			try {
				bd.favTable(id);
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
