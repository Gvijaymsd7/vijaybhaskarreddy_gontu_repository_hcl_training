package com.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bean.Book;
import com.dao.BookDatabase;

public class Books1 {
	/**
	 * Servlet implementation class Book1Servlet
	 */
	@WebServlet("/book1")
	public class Book1Servlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		/**
		 * @see HttpServlet#HttpServlet()
		 */
		public Book1Servlet() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			BookDatabase database = new BookDatabase();
			try {
				List<Book> books = database.getAllItems();
				RequestDispatcher rd = request.getRequestDispatcher("book1.jsp");
				request.setAttribute("list", books);
				rd.forward(request, response);
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
	}

}
