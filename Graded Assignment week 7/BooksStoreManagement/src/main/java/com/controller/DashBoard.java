package com.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class DashBoard {
	/**
	 * Servlet implementation class DashboardServlet
	 */
	@WebServlet("/dashboard")
	public class DashboardServlet extends HttpServlet {
		private static final long serialVersionUID = 1L;

		/**
		 * @see HttpServlet#HttpServlet()
		 */
		public DashboardServlet() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			HttpSession session =request.getSession();
			String email=(String) session.getAttribute("email");
			response.setHeader("Cache-Control", "no-cache,no-store,must-revalidate");
			response.setHeader("Pragma", "no-cache");//http1.0
			response.setHeader("Pragma", "0");//proxies

			if(email!=null) {
				RequestDispatcher rd = request.getRequestDispatcher("dashboard.jsp");
				rd.forward(request, response);
			}
			else
				response.sendRedirect("login.jsp");
		}
		/**
		 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
		 */
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			PrintWriter out = response.getWriter();
			String email = request.getParameter("email");
			response.setContentType("text/html");
			out.println("<html><head><style>h1{color:red;}</style></head><body>");
			out.println("<h1>Welcome "+email+"</h1>");
			out.println("<h3><a href='books'>List Of Books</a></h3>");
			out.println("<h3><a href='logout'>Logout</a></h3>");

			out.println("</body></html>");

		}
	}

}
