package com.assignment.books.bean;

public class Book {
	private int bookId;
	private String bookTitle;
	private String bookgenre;
	private String image;
	
	public Book() {
		
	}
	
	public Book(int bookId, String bookTitle, String bookgenre, String image) {
		this.bookId = bookId;
		this.bookTitle = bookTitle;
		this.bookgenre = bookgenre;
		this.image = image;
	}

	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public String getBookTitle() {
		return bookTitle;
	}
	public void setBookTitle(String bookTitle) {
		this.bookTitle = bookTitle;
	}
	public String getBookgenre() {
		return bookgenre;
	}
	public void setBookgenre(String bookgenre) {
		this.bookgenre = bookgenre;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Book [bookId=" + bookId + ", bookTitle=" + bookTitle + ", bookgenre=" + bookgenre + ", image=" + image
				+ "]";
	}
	
	
	
	
}
