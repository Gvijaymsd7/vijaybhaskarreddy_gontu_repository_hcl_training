package com.assignment.books.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.assignment.books.bean.Book;
import com.assignment.books.service.BookService;

@WebServlet("/BooksController")
public class BookController extends HttpServlet{
	public BookController() {
		super();
	}

	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BookService bs = new BookService();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter pw = response.getWriter();
		int bookid=Integer.parseInt(request.getParameter("bookid"));
		String booktitle = request.getParameter("booktitle");
		String bookgenre = request.getParameter("bookgenre");
		String image=request.getParameter("image");
		
		Book bb = new Book();
		bb.setBookTitle(booktitle);
		bb.setBookgenre(bookgenre);
		bb.setBookId(bookid);
		bb.setImage(image);
		
		BookService bs = new BookService();
		String res = bs.storeBook(bb);
		pw.println(res);
		
		
	}


}

