package com.assignment.books.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.assignment.books.bean.Book;

public class BookDao {
	private Connection con;

	private String query;
    private PreparedStatement pst;
    private ResultSet rs;
    public BookDao(Connection con) {
		
		this.con = con;
	}

    

	public List<Book> getAllBooks() {
        List<Book> listOfBooks = new ArrayList<>();
        try {

            query = "select * from books";
            pst =con.prepareStatement(query);
            rs = pst.executeQuery();

            while (rs.next()) {
            	Book bb = new Book();
                bb.setBookId(rs.getInt("bookid"));
                bb.setBookTitle(rs.getString("booktitle"));
                bb.setBookgenre(rs.getString("bookgenre"));
                bb.setImage(rs.getString("image"));

                listOfBooks.add(bb);
            }

        } catch (SQLException e) {
            
            System.out.println(e);
        }
        return listOfBooks;
    }

	

}

