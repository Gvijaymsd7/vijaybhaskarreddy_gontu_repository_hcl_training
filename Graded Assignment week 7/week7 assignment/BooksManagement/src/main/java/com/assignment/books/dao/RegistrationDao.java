package com.assignment.books.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.assignment.books.bean.Registration;
import com.assignment.books.dbresourse.DbConnection;

public class RegistrationDao {
	
	
	public int storeRegister(Registration reg) {
		try {
			Connection con = DbConnection.getConnection();
			PreparedStatement pstmt = con.prepareStatement("insert into registration values(?,?)");
			pstmt.setString(1, reg.getUserName());
			pstmt.setString(2, reg.getPassWord());
			pstmt.setNString(3, reg.getEmail());
			
			return pstmt.executeUpdate();
		} catch (Exception e) {
			System.out.println("Store Method Exception "+e);
			return 0;
		}
	}
}
