<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style >
body{
margin: 0;
padding: 0;
text-align: center;
background-color: #DBF9FC;
}
</style>


 <link href="https://fonts.googleapis.com/css?family=ZCOOL+XiaoWei" rel="stylesheet">
 <link href="css/style.css" rel="stylesheet" type="text/css"/>

</head>

<body style="text-align:center;">
<h1>WelCome To BookSore</h1>
<br>
<a href="login.jsp"><button>Login</button></a>
<a href="registration.jsp"><button>Signin</button></a>
<br>
<br>
	<div class="container">
		<div class="card-header my-3">All Books Without Login</div>
		<div class="row">
		
		<style>
		.Kusuma{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Lakshmi{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Anitha{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Subhadra{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.DurgaRao{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Shine{
		float:left;
		background-color: lightgrey;
		height:520px;
  		width: 300px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>		
		<div class="Kusuma">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="BookImage" src="image/clarissa.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"Clarissa"</h5>
						<h6 class="bookgenre">BookGenre:"Horror"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Random House Trade Paperbacks"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a> 	
							<a href="readlater.jsp"><button>ReadLater Books</button></a>					</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Lakshmi">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/frankenstein.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"Frankenstein"</h5>
						<h6 class="bookgenre">BookGenre:"Horror"</h6>
						<h6 class="bookPublisher"> BookPublisher:"HarperCollins"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Anitha">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/aliceinwonderland.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"Alice in Wonderland"</h5>
						<h6 class="bookgenre">Bookgenre:"cartoon"</h6>
						<h6 class="bookPublisher"> BookPublisher:"JAICO BOOKS"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Subhadra">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/sybil.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"Sybil"</h5>
						<h6 class="bookgenre">Bookgenre:"Horror"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Simon & Schuster"</h6>
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="DurgaRao">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="BookImage" src="image/richdadpoordad.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:" Rich Dad Poor Dad"</h5>
						<h6 class="bookgenre">BookGenre:"Motivational""</h6>
						<h6 class="bookPublisher"> BookPublisher:"John Murray Press"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a>
							<a href="readlater.jsp"><button>ReadLater Books</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Shine">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/littlewomen.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"Little women"</h5>
						<h6 class="bookgenre">BookGenre:"Lady Oriented"</h6>
						<h6 class="bookPublisher"> BookPublisher:"Simon & Schuster"</h6>
						<div class="mt-3 d-flex justify-content-between"><br>
							<a class="btn btn-dark" href="like.jsp"><button>Like</button></a> 
							<a href="readlater.jsp"><button>ReadLater Books</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>
			
</body>
</html>
