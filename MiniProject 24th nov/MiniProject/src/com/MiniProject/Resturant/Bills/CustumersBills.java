package com.MiniProject.Resturant.Bills;
    import com.MiniProject.Resturant.menu.*;
    import java.time.LocalDateTime;
	import java.util.*;


	public class CustumersBills 
	{
		private String name;
		private List<ResturantMenu> items;
		private double cost;
		private Date time;
		
		public CustumersBills() { }
	    
		public CustumersBills(String name, List<ResturantMenu> items, double cost, Date time) throws IllegalArgumentException {
			super();
			
			this.name = name;
			this.items = items;
			if(cost<0)
			{
				throw new IllegalArgumentException("exception occured, cost cannot less than zero");
			}
			this.cost = cost;
			this.time = time;
			
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		
		public List<ResturantMenu> getMenu() {
			return items;
		}
		public void setMenu(List<ResturantMenu> selectedMenu) {
			this.items = selectedMenu;
		}
		
		public double getCost() {
			return cost;
		}
		public void setCost(double cost) {
			this.cost = cost;
		}
		
		public Date getTime() {
			return time;
		}
		public void setTime(Date date) {
			this.time = date;
		}
}
