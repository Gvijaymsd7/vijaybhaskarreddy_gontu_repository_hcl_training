import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BookServiceService } from '../service/book-service.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styles: [
  ]
})

export class AddBookComponent implements OnInit {
  name:any;

  book=new FormGroup({
    title:new FormControl(''),
    total_pages:new FormControl(''),
    rating:new FormControl(''),
    isbn:new FormControl('')
  })

  myBookServiceService: BookServiceService;
  myRouter: Router;
  myActivatedRoute:ActivatedRoute;
  constructor(activatedRoute:ActivatedRoute, router: Router, bookServiceService: BookServiceService){
    this.myRouter= router;
    this.myActivatedRoute=activatedRoute;
    this.myBookServiceService=bookServiceService;
   }

  ngOnInit(): void {
    this.name=this.myActivatedRoute.snapshot.paramMap.get('name');
  }
  
  addBook(){
    let addBook = {  
      title:this.book.value.title,
      total_pages:this.book.value.total_pages,
      rating:this.book.value.rating,
      isbn:this.book.value.isbn 
    }
    
    this.myBookServiceService.create(addBook).subscribe(result => this.gotoAdminPage());
  }

  gotoAdminPage() {
    this.myRouter.navigate(['/admin',this.name]);
  }
}
