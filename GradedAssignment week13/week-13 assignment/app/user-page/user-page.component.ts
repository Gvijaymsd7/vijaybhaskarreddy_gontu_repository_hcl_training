import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { BookServiceService } from 'src/app/service/book-service.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

 
  myActivatedRoute:ActivatedRoute;

  myRouter:Router;

  myBookServiceService: BookServiceService;

  name:any;

  bookList:any = [];

  constructor(activatedRoute:ActivatedRoute, router:Router, bookServiceService: BookServiceService) {
    this.myActivatedRoute=activatedRoute;
    this.myRouter=router;
    this.myBookServiceService=bookServiceService;
   }

   listBooks(){
    this.myBookServiceService.list().subscribe((response)=>{
      this.bookList = response;
    },(error=>{

    }));
  }

  ngOnInit(): void {
    this.name=this.myActivatedRoute.snapshot.paramMap.get('name');
    this.listBooks();
  }
}
