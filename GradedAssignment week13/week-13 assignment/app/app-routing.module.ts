import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { BookComponent } from 'src/app/book/book.component';
import { AddBookComponent } from './add-book/add-book.component';
import { UpdateBookComponent } from './update-book/update-book.component';
import { AdminComponent } from 'src/app/admin/admin.component';
import {AdminRegistrationComponent} from 'src/app/admin-registration/admin-registration.component'
import {AdminLoginComponent} from 'src/app/admin-login/admin-login.component'
import { UserComponent } from 'src/app/user/user.component';
import { UserPageComponent } from './user-page/user-page.component'; 
import {UserRegistrationComponent} from 'src/app/user-registration/user-registration.component'
import {UserLoginComponent} from 'src/app/user-login/user-login.component'

const routes: Routes = [
              {path:'',redirectTo:'books',pathMatch:'full'},
              {path:"books",component:BookComponent },
              {path:"add-book/:name",component:AddBookComponent},
              {path:"update-book/:book_id/:title/:total_pages/:rating/:isbn/:name",component:UpdateBookComponent},
              {path:"admin/:name",component:AdminComponent},
              {path:"admin-registration",component:AdminRegistrationComponent},
              {path:"admin-login",component:AdminLoginComponent},
              {path:"users/:name",component:UserComponent},
              {path:"user-page/:name",component:UserPageComponent},
              {path:"user-registration",component:UserRegistrationComponent},
              {path:"user-login",component:UserLoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
