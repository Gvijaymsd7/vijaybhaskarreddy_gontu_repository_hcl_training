import { Component, OnInit } from '@angular/core';
import { BookServiceService } from '../service/book-service.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent implements OnInit {
    name:any;

  book=new FormGroup({
    book_id:new FormControl(''),
    title:new FormControl(''),
    total_pages:new FormControl(''),
    rating:new FormControl(''),
    isbn:new FormControl('')
  })

  myBookServiceService: BookServiceService;

  myActivatedRoute:ActivatedRoute;

  myRoute:Router;

  constructor(activatedRoute:ActivatedRoute, route:Router, bookServiceService: BookServiceService){
    this.myActivatedRoute=activatedRoute;
    this.myRoute=route;
    this.myBookServiceService=bookServiceService;
   }

  ngOnInit(): void {
    this.name=this.myActivatedRoute.snapshot.paramMap.get('name');
  }

  updateBook(){
    let data = {
      book_id:this.book.value.book_id,
      title:this.book.value.title,
      total_pages:this.book.value.total_pages,
      rating:this.book.value.rating,
      isbn:this.book.value.isbn 
    }
    this.myBookServiceService.update(data).subscribe(result => this.gotoAdminPage());
  }

  gotoAdminPage() {
    this.myRoute.navigate(['/admin',this.name]);
  }
}
