create database movies_vijaybhaskarreddy;

use movies_vijaybhaskarreddy;



create table moviesintheaters(
id int(5) primary key,
title varchar(20),year int(4),
contentRating varchar(5),
duration varchar(10),
releaseDate varchar(12),
averageRating int(2),originalTitle varchar(20),
imdbRating int(2));


create table moviesintheaters_genres_rating(
gid int(2) primary key,
genres varchar(15),
rating int(2),
id int(2),foreign key(id) references moviesintheaters(id));


insert into moviesintheaters values(1,'black panther',2018,' ','pt15m','2018-02-16',0,' ',0);
insert into moviesintheaters values(2,'grottmannen dug',2018,' ','pt10m','2017-02-26',0,' ',1);
insert into moviesintheaters values(3,'aiyaary',2018,' ','pt17m','2018-02-16',0,' ',2);
insert into moviesintheaters values(4,'black panther',2019,' ','pt15m','2018-05-06',0,' ',3);
insert into moviesintheaters values(5,'the party',2017,' ','pt12m','2017-01-16',0,' ',4);

insert into moviesintheaters_genres_rating values(11,'action',4,1);
insert into moviesintheaters_genres_rating values(12,'adventure',3,1);
insert into moviesintheaters_genres_rating values(13,'sci-fi',5,1);
insert into moviesintheaters_genres_rating values(14,'animation',7,2);
insert into moviesintheaters_genres_rating values(15,'adventure',4,2);
insert into moviesintheaters_genres_rating values(16,'comedy',7,2);
insert into moviesintheaters_genres_rating values(17,'action',2,3);
insert into moviesintheaters_genres_rating values(18,'crime',3,3);
insert into moviesintheaters_genres_rating values(19,'drema',7,3);
insert into moviesintheaters_genres_rating values(20,'crime',5,4);
insert into moviesintheaters_genres_rating values(21,'action',2,4);
insert into moviesintheaters_genres_rating values(22,'animation',3,4);
insert into moviesintheaters_genres_rating values(23,'action',4,5);
insert into moviesintheaters_genres_rating values(24,'advanture',6,5);
insert into moviesintheaters_genres_rating values(25,'comedy',5,5);

select * from moviesintheaters;
select * from moviesintheaters_genres_rating; 
select m.*,m1.* from  moviesintheaters m,moviesintheaters_genres_rating m1 where m.id=m1.id;




create table moviescoming(
id int(5) primary key,
title varchar(20),year int(4),
contentRating varchar(5),
duration varchar(10),
releaseDate varchar(12),
averageRating int(2),originalTitle varchar(20),
imdbRating int(2));


create table moviescoming_genres_rating(
gid int(2) primary key,
genres varchar(15),
rating int(2),
id int(2),foreign key(id) references moviescoming(id));


insert into moviescoming values(1,'game night',2018,' ','pt12m','2018-02-26',0,' ',0);
insert into moviescoming values(2,'harri potter',2017,' ','pt11m','2017-02-26',1,' ',1);
insert into moviescoming values(3,'area x',2018,' ','pt17m','2018-02-16',0,' ',2);
insert into moviescoming values(4,'black panther',2019,' ','pt15m','2018-05-06',0,' ',3);
insert into moviescoming values(5,'the lodgers',2017,' ','pt12m','2017-01-16',0,' ',4);

insert into moviescoming_genres_rating values(11,'action',4,1);
insert into moviescoming_genres_rating values(12,'animation',3,1);
insert into moviescoming_genres_rating values(13,'comedy',2,1);
insert into moviescoming_genres_rating values(14,'crime',5,2);
insert into moviescoming_genres_rating values(15,'action',4,2);
insert into moviescoming_genres_rating values(16,'animation',4,2);
insert into moviescoming_genres_rating values(17,'drama',5,3);
insert into moviescoming_genres_rating values(18,'advanture',4,3);
insert into moviescoming_genres_rating values(19,'action',2,3);
insert into moviescoming_genres_rating values(20,'comedy',4,4);
insert into moviescoming_genres_rating values(21,'anamitation',6,4);
insert into moviescoming_genres_rating values(22,'drama',6,4);
insert into moviescoming_genres_rating values(23,'animantion',2,5);
insert into moviescoming_genres_rating values(24,'advanture',3,5);
insert into moviescoming_genres_rating values(25,'comedy',6,5);


select * from moviescoming;
select * from moviescoming_genres_rating; 
select m.*,m1.* from  moviescoming m,moviescoming_genres_rating m1 where m.id=m1.id;




create table topratingindian(
id int(5) primary key,
title varchar(20),year int(4),
contentRating varchar(5),
duration varchar(10),
releaseDate varchar(12),
averageRating int(2),originalTitle varchar(20),
imdbRating int(2));


create table topratingindian_genres_rating(
gid int(2) primary key,
genres varchar(15),
rating int(2),
id int(2),foreign key(id) references topratingindian(id));


insert into topratingindian values(1,'bhahubali',2017,' ','pt15m','2017-02-16',3,' ',0);
insert into topratingindian values(2,'dongal',2018,' ','pt15m','2018-02-16',2,' ',0);
insert into topratingindian values(3,'ms dhoni',2018,' ','pt15m','2018-02-16',4,' ',0);
insert into topratingindian values(4,'rrr',2021,' ','pt15m','2021-02-16',5,' ',0);
insert into topratingindian values(5,'puspha',2021,' ','pt15m','2021-02-16',4,' ',0);

insert into topratingindian_genres_rating values(11,'action',4,1);
insert into topratingindian_genres_rating values(12,'animation',3,1);
insert into topratingindian_genres_rating values(13,'comedy',2,1);
insert into topratingindian_genres_rating values(14,'crime',5,2);
insert into topratingindian_genres_rating values(15,'action',4,2);
insert into topratingindian_genres_rating values(16,'animation',4,2);
insert into topratingindian_genres_rating values(17,'drama',5,3);
insert into topratingindian_genres_rating values(18,'advanture',4,3);
insert into topratingindian_genres_rating values(19,'action',2,3);
insert into topratingindian_genres_rating values(20,'comedy',4,4);
insert into topratingindian_genres_rating values(21,'anamitation',6,4);
insert into topratingindian_genres_rating values(22,'drama',6,4);
insert into topratingindian_genres_rating values(23,'animantion',2,5);
insert into topratingindian_genres_rating values(24,'advanture',3,5);
insert into topratingindian_genres_rating values(25,'comedy',6,5);

select * from topratingindian;
select * from topratingindian_genres_rating; 
select m.*,m1.* from  topratingindian m,topratingindian_genres_rating m1 where m.id=m1.id;






create table topratingmovies(
id int(5) primary key,
title varchar(20),year int(4),
contentRating varchar(5),
duration varchar(10),
releaseDate varchar(12),
averageRating int(2),originalTitle varchar(20),
imdbRating int(2));


create table topratingmovies_genres_rating(
gid int(2) primary key,
genres varchar(15),
rating int(2),
id int(2),foreign key(id) references topratingmovies(id));


insert into topratingmovies values(1,'iron man',2018,' ','pt15m','2018-02-16',2,' ',0);
insert into topratingmovies values(2,'thor',2018,' ','pt16m','2018-02-16',0,' ',0);
insert into topratingmovies values(3,'black windo',2017,' ','pt12m','2017-02-16',5,' ',0);
insert into topratingmovies values(4,'black panther',2018,' ','pt13m','2018-02-16',0,' ',0);
insert into topratingmovies values(5,'captian marval',2017,' ','pt15m','2017-02-16',6,' ',0);

insert into topratingmovies_genres_rating values(11,'action',4,1);
insert into topratingmovies_genres_rating values(12,'animation',3,1);
insert into topratingmovies_genres_rating values(13,'comedy',2,1);
insert into topratingmovies_genres_rating values(14,'crime',5,2);
insert into topratingmovies_genres_rating values(15,'action',4,2);
insert into topratingmovies_genres_rating values(16,'animation',4,2);
insert into topratingmovies_genres_rating values(17,'drama',5,3);
insert into topratingmovies_genres_rating values(18,'advanture',4,3);
insert into topratingmovies_genres_rating values(19,'action',2,3);
insert into topratingmovies_genres_rating values(20,'comedy',4,4);
insert into topratingmovies_genres_rating values(21,'anamitation',6,4);
insert into topratingmovies_genres_rating values(22,'drama',6,4);
insert into topratingmovies_genres_rating values(23,'animantion',2,5);
insert into topratingmovies_genres_rating values(24,'advanture',3,5);
insert into topratingmovies_genres_rating values(25,'comedy',6,5);

select * from topratingmovies;
select * from topratingmovies_genres_rating; 
select m.*,m1.* from  topratingmovies m,topratingmovies_genres_rating m1 where m.id=m1.id;




