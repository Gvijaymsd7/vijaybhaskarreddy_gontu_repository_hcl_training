package com.greatlearning.bean;

public class Movies 
{
 
	int id;
	String title;
	int year;
	int contentRating;
	String duration;
	String releaseDate;
	int averageRating;
	String originalTitle;
	int imdbRating;
	
	public Movies() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Movies(int id, String title, int year, int contentRating, String duration, String releaseDate,
			int averageRating, String originalTitle, int imdbRating) {
		super();
		this.id = id;
		this.title = title;
		this.year = year;
		this.contentRating = contentRating;
		this.duration = duration;
		this.releaseDate = releaseDate;
		this.averageRating = averageRating;
		this.originalTitle = originalTitle;
		this.imdbRating = imdbRating;
		
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getContentRating() {
		return contentRating;
	}
	public void setContentRating(int contentRating) {
		this.contentRating = contentRating;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public String getOriginalTitle() {
		return originalTitle;
	}
	public void setOriginalTitle(String originalTitle) {
		this.originalTitle = originalTitle;
	}
	public int getImdbRating() {
		return imdbRating;
	}
	public void setImdbRating(int imdbRating) {
		this.imdbRating = imdbRating;
	
	}
	
	@Override
	public String toString() {
		return "Movies [id=" + id + ", title=" + title + ", year=" + year + ", contentRating=" + contentRating
				+ ", duration=" + duration + ", releaseDate=" + releaseDate + ", averageRating=" + averageRating
				+ ", originalTitle=" + originalTitle + ", imdbRating=" + imdbRating + "]";
	}
	
	
	
}
